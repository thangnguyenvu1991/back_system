import 'dart:async';

import 'package:flutter/services.dart';

class BackSystem {
  static const MethodChannel _channel = MethodChannel('back_system');

  /// Calls the platform-specific function to send the app to the background
  static Future<void> moveTaskToBack() async {
    await _channel.invokeMethod('sendToBackground');
  }
}
